using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Door : MonoBehaviour
{
    [SerializeField] private float speed;
    private float angle;
    private Vector3 direction;
    private bool open;
    private bool canOpen;

    [SerializeField] private Text interactionText; // Asigna el objeto Text desde el Inspector

    void Start()
    {
        angle = transform.eulerAngles.y;
        interactionText.enabled = false; // Desactiva el texto al inicio
    }

    void Update()
    {
        if (Mathf.Round(transform.eulerAngles.y) != angle)
        {
            transform.Rotate(direction * speed);
        }

        if (Input.GetKeyDown(KeyCode.E) && open == true && canOpen == false)
        {
            angle = 90;
            direction = Vector3.up;
            canOpen = true;
        }
        else if (Input.GetKeyDown(KeyCode.E) && open == true && canOpen == true)
        {
            angle = 0;
            direction = Vector3.down;
            canOpen = false;
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            open = true;
            interactionText.enabled = true; // Activa el texto al estar en el �rea
            interactionText.text = "Press 'E' to open door";
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            open = false;
            canOpen = false;
            interactionText.enabled = false; // Desactiva el texto al salir del �rea
        }
    }
}
