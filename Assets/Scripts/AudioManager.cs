using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;
    public static AudioManager instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        foreach (Sound s in sounds)
        {
            s.AudioSource = gameObject.AddComponent<AudioSource>();
            s.AudioSource.clip = s.SoundClip;
            s.AudioSource.volume = s.Volume;
            s.AudioSource.pitch = s.pitch;
            s.AudioSource.loop = s.loop;
        }
    }

    public void PlaySound(string name)
    {
        Sound s = Array.Find(sounds, s => s.Name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found.");
            return;
        }
        else
        {
            s.AudioSource.Play();
        }
    }

    public void PauseSound(string name)
    {
        Sound s = Array.Find(sounds, s => s.Name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found.");
            return;
        }
        else
        {
            s.AudioSource.Pause();
        }
    }
}
