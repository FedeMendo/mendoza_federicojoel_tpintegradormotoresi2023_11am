using System.Collections;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [Header("Enemy Settings")]
    public GameObject enemyPrefab;
    public float initialEnemySpeed = 3f;
    public float speedIncreasePerRound = 0.5f;

    [Header("Spawn Settings")]
    public int initialSpawnDelay = 2;
    public float spawnInterval = 5f;
    public Transform[] spawnPoints;

    private bool isSpawning = false;
    private bool isFirstRound = true;

    private void Start()
    {
        StartCoroutine(SpawnEnemiesRoutine());
    }

    private IEnumerator SpawnEnemiesRoutine()
    {
        yield return new WaitForSeconds(initialSpawnDelay);

        while (true)
        {
            if (!isSpawning)
            {
                int enemiesToSpawn = GameManager.instance.currentEnemiesPerRound;
                StartCoroutine(SpawnRound(enemiesToSpawn));
            }

            yield return new WaitForSeconds(spawnInterval);
        }
    }

    private IEnumerator SpawnRound(int enemiesToSpawn)
    {
        isSpawning = true;

        for (int i = 0; i < enemiesToSpawn; i++)
        {
            SpawnEnemy();
            yield return new WaitForSeconds(1f);
        }

        yield return WaitForEnemiesToBeDefeated();

        GameManager.instance.EndRound();
        UpdateEnemyCountForNextRound();

        isSpawning = false;
    }

    private IEnumerator WaitForEnemiesToBeDefeated()
    {
        while (GameObject.FindWithTag("Enemy") != null)
        {
            yield return null;
        }

        yield return new WaitForSeconds(2f);
    }

    private void SpawnEnemy()
    {
        Transform spawnPoint = GetRandomSpawnPoint();
        GameObject newEnemy = Instantiate(enemyPrefab, spawnPoint.position, Quaternion.identity);
        newEnemy.SetActive(true);

        SetupEnemy(newEnemy);
    }

    private Transform GetRandomSpawnPoint()
    {
        return spawnPoints[Random.Range(0, spawnPoints.Length)];
    }

    private void SetupEnemy(GameObject enemy)
    {
        EnemyControl enemyControl = enemy.GetComponent<EnemyControl>();
        if (enemyControl != null)
        {
            enemyControl.OnDefeated += HandleEnemyDefeated;
            float enemySpeed = initialEnemySpeed + (GameManager.instance.actualRound - 1) * speedIncreasePerRound;
            enemyControl.SetSpeed(enemySpeed);
        }
    }

    private void HandleEnemyDefeated()
    {
        GameManager.instance.ZombieDefeat();
    }

    private void UpdateEnemyCountForNextRound()
    {
        if (isFirstRound)
        {
            isFirstRound = false;
        }
        else
        {
            GameManager.instance.currentEnemiesPerRound++;
        }
    }
}
