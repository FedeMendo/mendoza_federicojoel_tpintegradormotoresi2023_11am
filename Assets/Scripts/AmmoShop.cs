using UnityEngine;
using UnityEngine.UI;

public class AmmoShop : MonoBehaviour
{
    [Header("Shop Settings")]
    [SerializeField] private int ammoCost = 150;
    [SerializeField] private int ammoAmount = 80;

    [Header("UI Elements")]
    [SerializeField] private Text interactionText;

    private bool isPlayerInsideCollider = false;

    private void Start()
    {
        // Ensure interaction text is hidden at the start
        interactionText.enabled = false;
    }

    private void Update()
    {
        if (isPlayerInsideCollider && Input.GetKeyDown(KeyCode.E))
        {
            AttemptBuyAmmo();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (IsPlayer(other))
        {
            ShowInteractionText();
            isPlayerInsideCollider = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (IsPlayer(other))
        {
            HideInteractionText();
            isPlayerInsideCollider = false;
        }
    }

    private bool IsPlayer(Collider collider)
    {
        return collider.CompareTag("Player");
    }

    private void ShowInteractionText()
    {
        interactionText.enabled = true;
        interactionText.text = $"Press 'E' to buy ammo [-{ammoCost} EXP]";
    }

    private void HideInteractionText()
    {
        interactionText.enabled = false;
    }

    private void AttemptBuyAmmo()
    {
        PlayerControl playerControl = FindObjectOfType<PlayerControl>();
        GameManager gameManager = FindObjectOfType<GameManager>();

        if (playerControl == null || gameManager == null)
        {
            Debug.LogWarning("PlayerControl or GameManager not found");
            return;
        }

        if (gameManager.GetPlayerExp() >= ammoCost)
        {
            gameManager.SubtractExp(ammoCost);
            playerControl.AddAmmo(ammoAmount);
        }
        else
        {
            Debug.Log("No tienes suficiente experiencia para comprar munici�n.");
        }
    }
}
