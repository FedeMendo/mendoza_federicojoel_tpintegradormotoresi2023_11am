using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerControl : MonoBehaviour
{
    [Header("Movement Settings")]
    public float normalSpeed = 5.0f;
    public float crouchSpeed = 2.0f;
    public float jumpMagnitude = 10.0f;
    public float dashDistance = 5.0f;
    public float dashDuration = 0.2f;
    public float dashCooldown = 3.0f;
    public float reloadTime = 1.5f;
    public float healingCooldown = 10.0f;

    [Header("Health and Ammo")]
    public int maxHP = 100;
    public int damageAmount = 10;
    public int maxAmmo = 15;
    public int ammoInReserve = 180;

    [Header("UI Elements")]
    public Image cooldownImage;
    public Text cooldownText;
    public Image healthBar;
    public Image deathImage;
    public Text ammoText;

    [Header("References")]
    public Camera firstPersonCamera;
    public GameObject projectile;
    public LayerMask groundLayer;
    public GameManager gameManager;

    [Header("Experience and Rounds")]
    public Text expText;
    public Text roundText;

    private CapsuleCollider col;
    private Rigidbody rb;
    private bool isCrouching = false;
    private bool isReloading = false;
    private bool isDashing = false;
    private bool isDashAvailable = true;
    private float dashCooldownTimer = 0f;
    private int currentHP;
    private int currentAmmo;

    private float originalHeight;
    private Vector3 originalCenter;

    void Start()
    {
        InitializeComponents();
        InitializePlayerStats();
        Cursor.visible = false; // Hide cursor at the start
        Cursor.lockState = CursorLockMode.Locked; // Lock cursor at the start
    }

    void Update()
    {
        if (Time.timeScale > 0)
        {
            HandleInput();
            UpdateCooldownUI();
        }
    }

    void InitializeComponents()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();

        if (col == null)
        {
            Debug.LogWarning("No CapsuleCollider found on Player. Please add a CapsuleCollider component.");
        }
        else
        {
            originalHeight = col.height;
            originalCenter = col.center;
        }
    }

    void InitializePlayerStats()
    {
        currentHP = maxHP;
        currentAmmo = maxAmmo;
        gameManager = FindObjectOfType<GameManager>();
    }

    void HandleInput()
    {
        HandleMovement();
        HandleShooting();
        HandleCrouching();
        HandleDash();
        HandleReload();
    }

    void HandleMovement()
    {
        float speed = isCrouching ? crouchSpeed : normalSpeed;
        float forwardMovement = Input.GetAxis("Vertical") * speed * Time.deltaTime;
        float sidewaysMovement = Input.GetAxis("Horizontal") * speed * Time.deltaTime;

        transform.Translate(sidewaysMovement, 0, forwardMovement);

        if (Input.GetKeyDown(KeyCode.Space) && IsOnGround())
        {
            rb.AddForce(Vector3.up * jumpMagnitude, ForceMode.Impulse);
            AudioManager.instance.PlaySound("Jump");
        }
    }

    void HandleShooting()
    {
        if (!isReloading && Input.GetMouseButtonDown(0) && currentAmmo > 0)
        {
            FireProjectile();
            AudioManager.instance.PlaySound("Shoot");
            currentAmmo--;
        }
    }

    void HandleCrouching()
    {
        if (Input.GetKey(KeyCode.C))
        {
            if (!isCrouching)
            {
                isCrouching = true;
                Crouch();
            }
        }
        else if (isCrouching)
        {
            isCrouching = false;
            StandUp();
        }
    }

    void HandleDash()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift) && isDashAvailable)
        {
            StartCoroutine(Dash());
        }
    }

    void HandleReload()
    {
        if (Input.GetKeyDown(KeyCode.R) && currentAmmo < maxAmmo && !isReloading)
        {
            StartCoroutine(Reload());
        }
    }

    IEnumerator Reload()
    {
        isReloading = true;
        int bulletsToReload = Mathf.Min(maxAmmo - currentAmmo, ammoInReserve);
        yield return new WaitForSeconds(reloadTime);

        currentAmmo += bulletsToReload;
        ammoInReserve -= bulletsToReload;
        isReloading = false;
    }

    IEnumerator Dash()
    {
        isDashing = true;
        float startTime = Time.time;
        Vector3 dashDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")).normalized;

        while (Time.time < startTime + dashDuration)
        {
            float dashSpeed = dashDistance / dashDuration;
            Vector3 worldDashDirection = transform.TransformDirection(dashDirection);
            transform.Translate(worldDashDirection * dashSpeed * Time.deltaTime, Space.World);
            yield return null;
        }

        isDashing = false;
        StartDashCooldown();
    }

    void FireProjectile()
    {
        Ray ray = firstPersonCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        GameObject projectileInstance = Instantiate(projectile, ray.origin, transform.rotation);
        Rigidbody projectileRb = projectileInstance.GetComponent<Rigidbody>();
        projectileRb.AddForce(firstPersonCamera.transform.forward * 70, ForceMode.Impulse);
        Destroy(projectileInstance, 1);
    }

    void Crouch()
    {
        if (col != null)
        {
            col.height = originalHeight / 2f;
            col.center = originalCenter / 2f;
        }
    }

    void StandUp()
    {
        if (col != null)
        {
            col.height = originalHeight;
            col.center = originalCenter;
        }
    }

    private bool IsOnGround()
    {
        if (col != null)
        {
            return Physics.Raycast(col.bounds.center, Vector3.down, col.bounds.extents.y + 0.1f, groundLayer);
        }
        return false;
    }

    public void TakeDamage(int damage)
    {
        currentHP -= damage;
        healthBar.fillAmount = currentHP / (float)maxHP;

        if (currentHP <= 0)
        {
            PlayerDeath();
        }
    }

    void PlayerDeath()
    {
        Time.timeScale = 0;
        deathImage.gameObject.SetActive(true);
        Cursor.visible = true; // Show cursor on death
        Cursor.lockState = CursorLockMode.None; // Unlock cursor on death
        StartCoroutine(RestartAfterDelay(3f));
    }

    IEnumerator RestartAfterDelay(float delay)
    {
        yield return new WaitForSecondsRealtime(delay);
        deathImage.gameObject.SetActive(false);
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        gameManager.ResetGame();
        Cursor.visible = false; // Hide cursor after restart
        Cursor.lockState = CursorLockMode.Locked; // Lock cursor after restart
    }

    void UpdateCooldownUI()
    {
        cooldownImage.fillAmount = dashCooldownTimer / dashCooldown;
        cooldownText.text = Mathf.Ceil(dashCooldownTimer).ToString();
        ammoText.text = $"{currentAmmo}/{ammoInReserve}";

        if (!isDashAvailable)
        {
            dashCooldownTimer -= Time.deltaTime;
            if (dashCooldownTimer <= 0)
            {
                isDashAvailable = true;
                dashCooldownTimer = 0;
            }
        }
    }

    void StartDashCooldown()
    {
        isDashAvailable = false;
        dashCooldownTimer = dashCooldown;
    }

    public void Heal(int amount)
    {
        currentHP = Mathf.Min(currentHP + amount, maxHP);
        healthBar.fillAmount = currentHP / (float)maxHP;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            TakeDamage(damageAmount);
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("ToxicSmoke"))
        {
            TakeDamage(1);
        }
    }

    void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            TakeDamage(1);
        }
    }

    public void AddAmmo(int amount)
    {
        ammoInReserve += amount;
    }
}
