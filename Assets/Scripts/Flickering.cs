using UnityEngine;

public class Flickering : MonoBehaviour
{
    public enum WaveForm
    { sin, tri, sqr, saw, inv, noise };
    public WaveForm waveFunction = WaveForm.sin;
    public float start = 0.0f;
    public float amplitude = 1.0f;
    public float phase = 0.0f;
    public float frequency = 0.5f;
    private Color originalColor;
    private Light lightSource;

    void Start()
    {
        lightSource = GetComponent<Light>();
        originalColor = lightSource.color;
    }

    void Update()
    {
        lightSource.color = originalColor * (EvaluateWave());
    }

    float EvaluateWave()
    {
        float x = (Time.time + phase) * frequency;
        float y;
        x = x - Mathf.Floor(x);
        switch (waveFunction)
        {
            case WaveForm.sin:
                y = Mathf.Sin(x * 2 * Mathf.PI);
                break;
            case WaveForm.tri:
                if (x < 0.5f)
                    y = 4.0f * x - 1.0f;
                else
                    y = -4.0f * x + 3.0f;
                break;
            case WaveForm.sqr:
                if (x < 0.5f)
                    y = 1.0f;
                else
                    y = -1.0f;
                break;
            case WaveForm.saw:
                y = x;
                break;
            case WaveForm.inv:
                y = 1.0f - x;
                break;
            case WaveForm.noise:
                y = 1f - (Random.value * 2);
                break;
            default:
                y = 1.0f;
                break;
        }
        return (y * amplitude) + start;
    }
}
