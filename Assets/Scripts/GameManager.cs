using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    [Header("Game Settings")]
    public int initialEnemiesPerRound = 5;
    public int enemiesPerRound = 10;

    [Header("Player Stats")]
    public int actualRound = 1;
    public int playerExp = 0;
    public int enemiesKilled = 0;
    public int currentEnemiesPerRound;
    private bool isRoundActive = false;

    private PlayerControl playerControl;

    void Awake()
    {
        HandleSingleton();
    }

    void Start()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        InitGame();
    }

    void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void HandleSingleton()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void InitGame()
    {
        LoadPlayerControlReference();
        UpdateUI();
        currentEnemiesPerRound = initialEnemiesPerRound;
    }

    void LoadPlayerControlReference()
    {
        playerControl = FindObjectOfType<PlayerControl>();
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        LoadPlayerControlReference();
        UpdateUI();
    }

    void UpdateUI()
    {
        if (playerControl != null)
        {
            UpdateRoundText();
            UpdateExpText();
        }
    }

    private void UpdateRoundText()
    {
        if (playerControl.roundText != null)
            playerControl.roundText.text = $"Round: {actualRound}";
    }

    private void UpdateExpText()
    {
        if (playerControl.expText != null)
            playerControl.expText.text = $"Exp: {playerExp}";
    }

    public void ZombieDefeat()
    {
        playerExp += Random.Range(5, 11);
        enemiesKilled++;

        if (isRoundActive && enemiesKilled >= enemiesPerRound)
        {
            StartCoroutine(EndRoundCoroutine());
        }
        UpdateUI();
    }

    IEnumerator EndRoundCoroutine()
    {
        isRoundActive = false;
        yield return new WaitForSeconds(4f);
        EndRound();
    }

    public void EndRound()
    {
        actualRound++;
        enemiesKilled = 0;
        UpdateUI();
    }

    public void ResetGame()
    {
        actualRound = 1;
        playerExp = 0;
        enemiesKilled = 0;
        UpdateUI();
    }

    public void StartRound()
    {
        isRoundActive = true;
    }

    public int GetPlayerExp() => playerExp;

    public void SubtractExp(int amount)
    {
        playerExp -= amount;
        UpdateUI();
    }
}
