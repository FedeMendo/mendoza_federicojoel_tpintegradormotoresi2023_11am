using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class IAEnemy : MonoBehaviour
{

    public Transform Objective;
    public float velocity;
    public NavMeshAgent IA;

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        IA.speed = velocity;
        IA.SetDestination(Objective.position);
    }
}
