using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileDamage : MonoBehaviour
{
    private GameObject owner;

    public void SetOwner(GameObject newOwner)
    {
        owner = newOwner;
    }

    void OnTriggerEnter(Collider other)
    {
        if (owner != null && other.CompareTag("Enemy"))
        {
            other.GetComponent<EnemyControl>().TakeDamage(20);

            Destroy(gameObject);
        }
    }
}
