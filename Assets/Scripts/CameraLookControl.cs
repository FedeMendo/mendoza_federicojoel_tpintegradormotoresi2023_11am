using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLookControl : MonoBehaviour
{
    Vector2 lookInput;
    Vector2 smoothV;

    public float sensitivity = 4.0f;
    public float smoothing = 2.0f;

    GameObject player;

    void Start()
    {
        player = this.transform.parent.gameObject;
    }

    void Update()
    {
        if(Time.timeScale > 0) 
        { 
            var mouseDelta = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

            mouseDelta = Vector2.Scale(mouseDelta, new Vector2(sensitivity * smoothing, sensitivity * smoothing));

            smoothV.x = Mathf.Lerp(smoothV.x, mouseDelta.x, 1f / smoothing);
            smoothV.y = Mathf.Lerp(smoothV.y, mouseDelta.y, 1f / smoothing);

            lookInput += smoothV;
            lookInput.y = Mathf.Clamp(lookInput.y, -90f, 90f);
            transform.localRotation = Quaternion.AngleAxis(-lookInput.y, Vector3.right);
            player.transform.localRotation = Quaternion.AngleAxis(lookInput.x, player.transform.up);
        }
    }
}