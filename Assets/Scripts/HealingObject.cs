using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class HealingObject : MonoBehaviour
{
    [Header("Healing Settings")]
    [SerializeField] private int healingAmount = 20;
    [SerializeField] private float healingCooldown = 15.0f;

    [Header("UI Elements")]
    [SerializeField] private Text interactionText;
    [SerializeField] private Text cooldownText;
    [SerializeField] private Image healingIcon;

    private bool canHeal = true;
    private bool playerInside = false;

    private void Start()
    {
        InitializeUI();
    }

    private void Update()
    {
        if (playerInside && Input.GetKeyDown(KeyCode.E) && canHeal)
        {
            HealPlayer();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            SetPlayerInside(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            SetPlayerInside(false);
        }
    }

    private void HealPlayer()
    {
        PlayerControl playerControl = FindPlayerControl();
        if (playerControl != null)
        {
            playerControl.Heal(healingAmount);
            StartCoroutine(HealingCooldown());
        }
    }

    private PlayerControl FindPlayerControl()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        return player != null ? player.GetComponent<PlayerControl>() : null;
    }

    private IEnumerator HealingCooldown()
    {
        canHeal = false;
        float cooldownTimer = healingCooldown;

        ShowCooldownUI();

        while (cooldownTimer > 0)
        {
            cooldownText.text = Mathf.Ceil(cooldownTimer).ToString();
            cooldownTimer -= Time.deltaTime;
            yield return null;
        }

        ResetCooldownUI();
        canHeal = true;
    }

    private void InitializeUI()
    {
        interactionText.enabled = false;
        cooldownText.enabled = false;
        healingIcon.enabled = true;
    }

    private void SetPlayerInside(bool inside)
    {
        playerInside = inside;
        interactionText.enabled = inside;
        interactionText.text = inside ? "Press 'E' to heal" : string.Empty;
    }

    private void ShowCooldownUI()
    {
        cooldownText.enabled = true;
        healingIcon.enabled = true;
    }

    private void ResetCooldownUI()
    {
        cooldownText.text = string.Empty;
        cooldownText.enabled = false;
        healingIcon.enabled = true;
    }
}
