using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject ObjectPauseMenu;
    public bool Pause = false;

    void Start()
    {
        
    }
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape)) 
        { 
            if (Pause == false) 
            {
                ObjectPauseMenu.SetActive(true);
                Pause = true;

                Time.timeScale = 0;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
        }
    }
    public void Resume() 
    {
        ObjectPauseMenu.gameObject.SetActive(false);
        Pause = false;
        Time.timeScale = 1;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
    public void GoMenu(string StartMenu)
    {
        SceneManager.LoadScene(StartMenu);
    }
    public void Quit()
    {
        Application.Quit();
    }
}
