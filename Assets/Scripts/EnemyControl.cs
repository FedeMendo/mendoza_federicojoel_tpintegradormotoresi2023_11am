using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.AI;

public class EnemyControl : MonoBehaviour
{
    public int currentHP;
    public int damageAmount;
    public event Action OnDefeated;
    public Transform Objective;
    public float velocity;
    public NavMeshAgent IA;

    void Start()
    {
        currentHP = 50;
        Objective = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void Update()
    {
        IA.speed = velocity;
        IA.SetDestination(Objective.position);
    }

    public void TakeDamage(int damage)
    {
        currentHP -= damage;
        if (currentHP <= 0)
        {
            Disappear();
        }
    }

    private void Disappear()
    {
        Destroy(gameObject);
        OnDefeated?.Invoke();
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<PlayerControl>().TakeDamage(damageAmount);
            GameManager.instance.ZombieDefeat();
        }
    }
    public void SetSpeed(float speed)
    {
        velocity = speed;
    }
}